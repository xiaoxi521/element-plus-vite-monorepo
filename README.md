# Element-Plus-Vite-Monorepo

#### 介绍

多项目 Monorepo 开发模式项目模板，vite + vue3 + element-plus + pinia

## Bootstrap project

With command

```bash
$ pnpm i
```

## Local development

1. With command

```shell
$ pnpm template:dev
```

will start the local development environment

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
